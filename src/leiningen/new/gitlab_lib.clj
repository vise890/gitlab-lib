(ns leiningen.new.gitlab-lib
  "Generate a library project with Gitlab-specific config."
  (:require [leiningen.core.main :as main]
            [leiningen.new.templates
             :refer
             [->files
              date
              group-name
              name-to-path
              project-name
              renderer
              sanitize-ns
              year]]))

(def render (renderer "gitlab-lib"))

(defn gitlab-lib
  "A leiningen template for a Clojure library hosted on gitlab.com

  It initializes support for doc pages, CI and CD to Clojars.

  **Requires** a group id in the project name: `lein new vise890/baz`

  ;; FIXME group id doesn't support dots!
  "
  [name]
  (let [nested-dirs (-> name
                        sanitize-ns
                        name-to-path)
        main-ns     (-> name
                        sanitize-ns
                        (str ".core"))
        group       (group-name name)
        data        {:raw-name    name
                     :group       group
                     :name        (project-name name)
                     :main-ns     main-ns
                     :nested-dirs nested-dirs
                     :year        (year)
                     :date        (date)}]
    (when (nil? group)
      (main/abort "A group id in the project name is required: (e.g. `lein new vise890/baz`)"))
    (main/info "Generating a project called" name "based on the 'gitlab-lib' template.")
    (->files data
             ["project.clj" (render "project.clj" data)]
             ["README.md" (render "README.md" data)]
             [".gitignore" (render "gitignore" data)]
             [".gitlab-ci.yml" (render "gitlab-ci.yml" data)]
             ["src/{{nested-dirs}}/core.clj" (render "core.clj" data)]
             ["test/{{nested-dirs}}/core_test.clj" (render "test.clj" data)]
             ["LICENSE" (render "LICENSE" data)]
             "resources")
    (main/info "You *must* setup the Clojars username and password on Gitlab for CD to work (secret env variables `LEIN_USERNAME` and `LEIN_PASSWORD`).")))
