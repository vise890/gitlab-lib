# gitlab-lib [![pipeline status](https://gitlab.com/vise890/gitlab-lib/badges/master/pipeline.svg)](https://gitlab.com/vise890/gitlab-lib/commits/master) [![Clojars Project](https://img.shields.io/clojars/v/vise890/gitlab-lib.svg)](https://clojars.org/vise890/gitlab-lib)

A leiningen template for a Clojure library hosted on gitlab.com

It initializes support for doc pages, CI and CD to Clojars.

**Requires** a group id in the project name: `lein new vise890/baz`

FIXME: group id doesn't support dots!

## Usage

```
lein new gitlab-lib vise890/foomatic
```


## License

Copyright © 2018 Martino Visintin

Distributed under the Eclipse Public License either version 1.0 or (at your
option) any later version.
