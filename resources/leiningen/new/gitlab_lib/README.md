# {{name}} [![pipeline status](https://gitlab.com/{{group}}/{{name}}/badges/master/pipeline.svg)](https://gitlab.com/{{group}}/{{name}}/commits/master)

FIXME

## [API Docs](https://{{group}}.gitlab.io/{{name}}/)

## Usage

[![Clojars Project](https://img.shields.io/clojars/v/{{group}}/{{name}}.svg)](https://clojars.org/{{group}}/{{name}})

```clojure
;;; project.clj
[{{raw-name}} "0.1.0-SNAPSHOT"]
```

```clojure
;; FIXME
```

## License

Copyright © {{year}} FIXME

Distributed under the Eclipse Public License either version 1.0 or (at your
option) any later version.
