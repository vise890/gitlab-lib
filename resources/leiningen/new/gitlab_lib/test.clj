(ns {{main-ns}}-test
  (:require [clojure.test :refer :all]
            [{{main-ns}} :as sut]))

(deftest add-test
  (is (= 3 (sut/add 1 2))))
