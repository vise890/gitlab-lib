(defproject {{raw-name}} "0.1.0-SNAPSHOT"

  :description "FIXME: write description"

  :url "https://gitlab.com/{{group}}/{{name}}"

  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/spec.alpha "0.2.176"]]

  :plugins [[lein-codox "0.10.6"]]

  :release-tasks [["vcs" "assert-committed"]
                  ["change" "version"
                   "leiningen.release/bump-version" "release"]
                  ["vcs" "commit"]
                  ["vcs" "tag"]
                  ["vcs" "push"]
                  ["change" "version"
                   "leiningen.release/bump-version"]
                  ["vcs" "commit"]]

  :profiles {:ci {:deploy-repositories
                  [["clojars" {:url           "https://clojars.org/repo"
                               :username      :env
                               :password      :env
                               :sign-releases false}]]}}

  :repl-options {:init-ns {{main-ns}}})
