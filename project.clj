(defproject gitlab-lib/lein-template "0.1.9-SNAPSHOT"

  :description "A leiningen template for a Clojure library hosted on gitlab.com"

  :url "https://gitlab.com/vise890/gitlab-lib"

  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}

  :plugins [[lein-codox "0.10.6"]]

  :release-tasks [["vcs" "assert-committed"]
                  ["change" "version"
                   "leiningen.release/bump-version" "release"]
                  ["vcs" "commit"]
                  ["vcs" "tag"]
                  ["vcs" "push"]
                  ["change" "version"
                   "leiningen.release/bump-version"]
                  ["vcs" "commit"]]

  :profiles {:ci {:deploy-repositories
                  [["clojars" {:url           "https://clojars.org/repo"
                               :username      :env
                               :password      :env
                               :sign-releases false}]]}}

  :eval-in-leiningen true)
